#include <string>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <set>
#include <iterator>
#include <math.h>
#include <algorithm>
#include <sstream>
#include <sys/time.h>

using namespace std;

/*global variable*/
int notSatisfiedCount = 0;			// global counter for continuous row not satisfied euclid condition

vector< vector<int> > allNewData;	//store discovery data from multiple csv file

int nDataGlobal = 0;				//Number of row data to earn from multiple csv file

// calculate Euclidean between array
float distEuclidean(
	vector<int> arr1,	// first array 
	vector<int> arr2,	// second array
	int d1				// first d1 elements (ignore primary element)
	);

// Parse csv data from input filename to an array data
vector< vector<int> > inputCsvToArray(
	const char *filename	// Name of csv file
	);

//Create a new row candidate from array data to put into euclid check
vector<int> createRowCandidate(
	vector< vector<int> > arr,	// input array of data
	int insertInd,				// current index of row on new file
	int j,						// a random number
	int d1,						// demension d1
	int d2,						// demension d2
	int column					// column to select data. T2 or T3 must be selected.
	);

// Append new row to a csv file with name filename
void appendRowToCsvFile(
	vector<int> row,		// Row data to append
	const char *fileName	// Name of file store row
	);

//create random number in range
int randomInRange(
	int min,	// min value
	int max		// max value
	);

//Find all euclidean satisfied row from an array data
vector< vector<int> > findAllRow(
	vector< vector<int> > arr,		// an array data input
	int d1,							// dimension d1
	int d2,							// dimension d2
	int col,						// colunm to select data. T2 or T3 must be selected
	int euclid,						// using only one file processing. threshold euclidean to check
	bool multiFile					// flag to indicate process one or more csv file. True: multiple csv file, otherwise: one csv file
	);			

// Parse parameter from file
vector<string> readParamTxt(
	const char *filename	// Name of file
	);

//convert string to integer for column
int convertStrToCol(
	string strColumn	// Name of column
	);

//Find all euclidean satisfied row from an array data from specified parameter
void findAllRowMultiCsv(
	vector<string> paramArr // parameter list
	);

//Heuristic metho to find euclidean threshold for findAllRowMultiCsv's parameter
float euclideanHeuristic(
	vector< vector<int> > arr,  // an array data input
	int d1,						// dimension d1
	int d2,						// dimension d2
	int col,					// colunm to select data. T2 or T3 must be selected
	int maxCount				// maximum of random row
	);
double dblTimeNow();

int main(int argc, char* argv[])
{
	// Check the number of parameters
	if (argc < 2) {
		// Tell the user how to run the program
		std::cerr << "Usage:" << argv[0] << " param_file" << std::endl
			<< "\t param_file: list of parameter with format:" << std::endl
			<< "\t - DataColumn: column_option (T2 or T3)" << std::endl
			<< "\t - Data1: d1" << std::endl
			<< "\t - Data2: d2" << std::endl
			<< "\t - nData: number of obtain data" << std::endl
			<< "\t - Euclidean_start: value of init euclidean" << std::endl
			<< "\t - OutputFilename: csv_output_file" << std::endl
			<< "\t - InputCSVFileName1: csv_input_file 1" << std::endl
			<< "\t - ......." << std::endl
			<< "\t - InputCSVFileName8: csv_input_file 8" << std::endl
			<< "\t for example:" << std::endl
			<< "\t\t DataColumn: T2" << std::endl
			<< "\t\t Data1: 10" << std::endl
			<< "\t\t Data2: 5" << std::endl
			<< "\t\t nData: 1000" << std::endl
			<< "\t\t Euclidean_start: 100" << std::endl
			<< "\t\t OutputFilename: abc_new.csv" << std::endl
			<< "\t\t InputCSVFileName1: abc_1.csv" << std::endl
			<< "\t\t InputCSVFileName2: abc_2.csv" << std::endl
			<< "\t\t InputCSVFileName3: abc_3.csv" << std::endl
			<< "\t\t InputCSVFileName4: abc_4.csv" << std::endl
			<< "\t\t InputCSVFileName5: abc_5.csv" << std::endl
			<< "\t\t InputCSVFileName6: abc_6.csv" << std::endl
			<< "\t\t InputCSVFileName7: abc_7.csv" << std::endl
			<< "\t\t InputCSVFileName8: abc_8.csv" << std::endl;
		;
		return 1;
	}

	string paramFile = argv[1];
	cout << "Loading param from file..." << endl;
	vector<string> paramLists = readParamTxt(paramFile.c_str()); //reading parameter list from file
	if (paramLists.size() >= 6)
	{
		cout << "Complete loading param from file. Starting find data..." << endl;
		findAllRowMultiCsv(paramLists);
	}

	return 0;

}

// calculate Euclidean between array
float distEuclidean(
	vector<int> arr1,	// first array 
	vector<int> arr2,	// second array
	int d1				// first d1 elements (ignore primary element)
	)
{
	float Sum = 0.0f;

	if (arr1.size() <= (unsigned)d1 || arr2.size() <= (unsigned)d1 || d1 < 1)
	{
		return -1;
	}


	for (int i = 1; i <= d1; i++)
	{
		Sum = Sum + pow((arr1[i] - arr2[i]), 2.0);
	}

	return sqrt(Sum);
}

// Parse csv data from input filename to an array data
vector< vector<int> > inputCsvToArray(
	const char *filename	// Name of csv file
	)
{
	ifstream in(filename);
	string line, field;
	int temp;					// temporatory value of each number in csv file
	vector< vector<int> > arr;  // the 2D array
	vector<int> v;              // array of values for one line only
	int i = 0;
	while (getline(in, line))   // get next line in file
	{
		if (i == 0)				// ignore first line
		{
			i++;
			continue;
		}
		v.clear();
		stringstream ss(line);

		while (getline(ss, field, ','))  // break line into comma delimitted fields
		{

			temp = atoi(field.c_str());	 // convert string to int number
			if (temp > 0)
			{
				v.push_back(temp);		// add each field to the 1D array
			}
		}
		if (v.size() > 2)
		{
			arr.push_back(v);			// add the 1D array to the 2D array
		}

	}

	return arr;
}

//Create a new row candidate from array data to put into euclid check
vector<int> createRowCandidate(
	vector< vector<int> > arr,	// input array of data
	int insertInd,				// current index of row on new file
	int j,						// a random number
	int d1,						// demension d1
	int d2,						// demension d2
	int column					// column to select data. T2 or T3 must be selected.
	)
{
	vector<int> row;  // the output array
	// check ahead
	if (column != 1 && column != 2)
	{
		return row;
	}

	if (j < 0 || d1 < 1 || d2 < 1 || arr.size() < 1 || (unsigned)(j + d1 + d2) > arr.size())
	{
		return row;
	}
	// insert index to row
	row.push_back(insertInd);

	// append next d1 elements from colum to row
	for (int i = 1; i <= d1; i++)
	{
		row.push_back(arr[j + i - 1][column]);
	}

	// append next d2 elements from colum to row
	for (int i = 1; i <= d2; i++)
	{
		row.push_back(arr[j + d1 + i - 1][column]);
	}

	// append last element
	row.push_back(arr[j][0]);

	return row;
}

// Append new row to a csv file with name filename
void appendRowToCsvFile(	
	vector<int> row,		// Row data to append
	const char *fileName	// Name of file store row
	)
{
	ofstream outputFile;
	outputFile.open(fileName, std::ofstream::out | std::ofstream::app);
	for (size_t j = 0; j < row.size(); ++j)
	{
		if (j == row.size() - 1)
		{
			outputFile << row[j] << std::endl;   // (separate fields by |)
		}
		else
		{
			outputFile << row[j] << ",";		// (separate fields by |)
		}
	}
	outputFile.close();
}

//create random number in range
int randomInRange(	
	int min,	// min value
	int max		// max value
	)
{
	return min + (std::rand() % (max - min + 1));
}


//Find all euclidean satisfied row from an array data
vector< vector<int> > findAllRow(
	vector< vector<int> > arr,		// an array data input
	int d1,							// dimension d1
	int d2,							// demension d2
	int col,						// colunm to select data. T2 or T3 must be selected
	int euclid,						// using only one file processing. threshold euclidean to check
	bool multiFile = false			// flag to indicate process one or more csv file. True: multiple csv file, otherwise: one csv file
	)			
{

	cout << "Start findAllRow function...." << endl;
	double t1 = dblTimeNow();
	vector< vector<int> > results; // array to store new row
	// if multiple csv processing, assign before new satisfied row collection to results
	if (multiFile && allNewData.size() > 0)
	{
		results = allNewData;
	}
	// check for column
	if (col != 1 && col != 2)
	{
		cout << "Function stop caused column error. Column value: " << col << endl;
		return results;
	}

	// check for demension
	if (d1 < 1 || d2 < 1 || arr.size() < 1 || (unsigned)d1 + d2 > arr.size() || euclid < 0)
	{
		cout << "Function stop caused demension or euclid error." << endl;
		return results;
	}

	float euclidTemp;						// euclidean temporatory
	int ind = 0;							// index of new row

	// if multiple csv file process then init ind to size of before row list
	if (multiFile && allNewData.size() > 0)
	{
		ind = allNewData.size();
	}
	vector<int> row;						// tempratory new row
	vector<int> oldRand;					// list of taken random j before
	bool firstRand = true;					// first time random

	// check
	bool stopFlag = false;					// flag stop: if true then stop while loop
	
	// multi csv file 1000 row not satisfied check
	bool currentSatified = false;			// flag to check continuous of euclid statement, using only for multiple csv file process
	cout << "Loop find new row with specified euclidean value..." << endl;
	while (!stopFlag)
	{
		if (oldRand.size() == arr.size())			// if random list fulfill in origin array
		{
			cout << "Loop find new row stop caused random size fit to origin data size" << endl;
			stopFlag = true;
			break;
		}

		bool isRand = false;						// flag to check first time random
		int rando;
		cout << "Loop find random number..." << endl;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size());  // random number in range
			cout << "random number candidate: " << rando << endl;
			if (firstRand)							// check firstime random
			{
				oldRand.push_back(rando);			// push random value to list taken random
				firstRand = false;					// update firstime random
				isRand = true;						// update 
			}
			else									// if not firstime random
			{
				// check new random existing in list taken radom
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}
		double t2 = dblTimeNow() - t1;
		t1 = dblTimeNow();
		cout << "Found random number: " << rando; 
		printf(" time: %f secs\n", t2);
		cout << "Create new row candidate with above random number" << endl;
		row = createRowCandidate(arr, ind, rando, d1, d2, col); // create new row candidate with found randoms
		double t3 = dblTimeNow() - t1;
		t1 = dblTimeNow();
		cout << "Row candidate size: " << row.size();
		printf(" time: %f secs\n", t3);
		if (row.size() < 1)										// if row cannot created
		{
			cout << "Function stop caused row candidate empty! " << endl;
			stopFlag = true; // stop 
			break;
		}
		if (ind == 0) // if first time insert new row to new csv file
		{
			cout << "Push row candidate to new data! " << endl;
			results.push_back(row);
			ind++;
		}
		else
		{
			bool insertPosible = true; // flag to check posible insert new row candidate to list data
			// loop to check euclidean from new row with before list data
			cout << "Check row candidate with all before new data by euclidean value! " << endl;
			for (size_t j = 0; j < results.size(); ++j)
			{
				cout << "Compare with before new data: " << results[j][0] << endl;
				euclidTemp = distEuclidean(row, results[j], d1); // caculate euclid distance 
				cout << "Euclidean distance: " << euclidTemp << endl;
				if (euclidTemp == -1 || (euclidTemp != -1 && euclidTemp < euclid)) // check if every before row has an euclid distance larger than euclidean min
				{
					cout << "Euclidean distance not satisfied! " << endl;
					insertPosible = false;
					break;
				}
			}

			double t4 = dblTimeNow() - t1;
			t1 = dblTimeNow();
			cout << "Complete checking row candidate" << " time: ";
			printf("%f secs\n", t4);

			if (insertPosible) // if insert posible
			{
				cout << "Append row candidate to new data list! " << endl;
				results.push_back(row);
				ind++;
				// if found row satisfied then update condition and satisfied counter
				if (multiFile) // if multiple csv processing
				{
					currentSatified = true; // reset euclidean satisfied state
					notSatisfiedCount = 0; // reset continuous row that not satisfied
					if (results.size() >= (unsigned)nDataGlobal) // check for enough data wanna to taken
					{
						cout << "Function stop because new data list fit nData! " << endl;
						stopFlag = true;
						break;
					}
				}
			}
			else		// if insert imposible
			{
				if (!multiFile) // if one csv file processing
				{
					stopFlag = true; // stop while loop imtermediate
					break;
				}
				else // if multiple csv process
				{
					notSatisfiedCount++; // increate number row that not satisfied euclid condition
					cout << "Row candidate not satisfied! Then update  number row that not satisfied euclid condition: " << notSatisfiedCount << endl;
					if (currentSatified)
					{
						currentSatified = false; // update satisfied euclid condition flag
					}
					if (notSatisfiedCount == 1000)
					{
						stopFlag = true;
						break;
					}
				}

			}
		}
	}

	cout << "Stop findAllRow function" << endl;
	//appendRowToCsvFile(oldRand, "random.csv");
	return results;
}

// Parse parameter from file
vector<string> readParamTxt(
	const char *filename	// Name of file
	)
{
	string line;
	vector<string> paramList;
	ifstream inputFile(filename);

	while (getline(inputFile, line))
	{
		istringstream ss(line);
		string var1, var2;
		ss >> var1 >> var2;
		if (!var2.empty()) // check value of param
		{
			paramList.push_back(var2);
		}
		cout << var1 << "\t" << var2 << endl;
	}
	return paramList;
}

//convert string to integer for column
int convertStrToCol(
	string strColumn	// Name of column
	)
{
	int colReturn = -1;
	if (strColumn.compare("T2") == 0)
	{
		colReturn = 1;
	}

	if (strColumn.compare("T3") == 0)
	{
		colReturn = 2;
	}
	return colReturn;
}

//Find all euclidean satisfied row from an array data from specified parameter
void findAllRowMultiCsv(
	vector<string> paramArr // parameter list
	)
{
	if (paramArr.size() < 6) // check param list
	{
		return;
	}
	int col = convertStrToCol(paramArr[0]); // convert column to number
	if (col == -1)
	{
		return;
	}
	int d1 = atoi(paramArr[1].c_str());		// convert d1 from param
	int d2 = atoi(paramArr[2].c_str());		// convert d2 from param
	int nData = atoi(paramArr[3].c_str()); // convert nData from param
	nDataGlobal = nData;					// assign nData to global variable to check in other function
	
	if (d1 <= 0 || d2 <= 0 || nData < 1)
	{
		return;
	}
	const char *outputFile = paramArr[4].c_str();	// output file to write discovery data into
	// read multi csv file into only one array data
	vector< vector<int> > allRawData;				// array of raw data collect from list input csv file
	bool firstInsert = true;						// flag to check first time inset in to raw data array
	/*collect all raw data from list input file*/
	
	double tOrigin = dblTimeNow();
	cout << "Starting load raw data from all input csv file..." << endl;
	for (int i = 5; (unsigned)i < paramArr.size(); i++)
	{
		
		cout << "Loading raw data from file: " << paramArr[i].c_str() << endl;
		vector< vector<int> > tempRawData = inputCsvToArray(paramArr[i].c_str()); // parse raw data from a csv file
		if (tempRawData.size() > 0) // check is has data
		{
			if (firstInsert)		// first time insert
			{
				firstInsert = false;
				allRawData = tempRawData; // assign 
			}
			else
			{
				allRawData.insert(allRawData.end(), tempRawData.begin(), tempRawData.end()); // append new raw data to allRawData
			}
		}
		double t2 = dblTimeNow() - tOrigin;
		tOrigin = dblTimeNow();
		cout << "Complete loading raw data from file: " << paramArr[i].c_str() << " number data: " << tempRawData.size() << " with loading time: ";
		printf("%f secs\n", t2); 
	}

	cout << "Complete loading all raw data from all file! " << endl;

	if (allRawData.size() < 1)
	{
		return;
	}

	cout << "Starting find euclidean max from all raw data..." << endl;
	float euclidean_start = euclideanHeuristic(allRawData, d1, d2, col, 1000);// convert euclidean start from param
	if (euclidean_start <= 0)
	{
		return;
	}
	double t3 = dblTimeNow() - tOrigin;
	tOrigin = dblTimeNow();
	cout << "Complete find euclidean max. Euclidean max: " << euclidean_start << " time: ";
	printf("%f secs\n", t3); 

	bool stop = false; // flag to check stop
	float euclidTemp = euclidean_start; // euclidean start
	vector< vector<int> > reTempArr; // array to store new discovery data
	/*find nData by adjust euclidean*/

	cout << "Starting find new row from all raw data..." << endl;
	while (!stop)
	{
		cout << "Finding new data with eucidean value: " << euclidTemp << endl;
		reTempArr = findAllRow(allRawData, d1, d2, col, euclidTemp, true); //find satisfied row from all raw data with euclid value

		double t4 = dblTimeNow() - tOrigin;
		tOrigin = dblTimeNow();
		cout << "Complete finding new data with eucidean value: " << euclidTemp << " update current number of data: " << reTempArr.size() << " with time: ";
		printf("%f secs\n", t4);
		if (reTempArr.size() > 1)			// if new data not empty
		{
			allNewData = reTempArr;			// update all new discovery data list
			if (allNewData.size() >= (unsigned)nData) // if enough data wanna to taken
			{
				stop = true;
				break;
			}
		}

		// if not enough data to earn, then update euclid value
		notSatisfiedCount = 0;			// reset number of not satisfied row
		euclidTemp = euclidTemp * 0.9; // update euclid value
	}

	double t5 = dblTimeNow() - tOrigin;
	tOrigin = dblTimeNow();
	cout << "Complete find new data, time: ";
	printf("%f secs\n", t5);

	// write all new discovery data to csv file
	cout << "Starting write new data to file: " << outputFile << endl;
	if (allNewData.size() > 0)
	{

		for (size_t j = 0; j < allNewData.size(); ++j)
		{
			appendRowToCsvFile(allNewData[j], outputFile);
		}
	}

	double t6 = dblTimeNow() - tOrigin;
	tOrigin = dblTimeNow();
	cout << "Complete write new data to file: " " time ";
	printf("%f secs\n", t6);

	cout << "Starting write euclidean (max, min) to file: " << endl;
	// write euclid value to file
	ofstream euclidFile;
	euclidFile.open("euclidean.txt", std::ofstream::out);
	euclidFile << "Euclidean Max: " << euclidean_start << "\t" << endl;
	euclidFile << " Euclidean Min: " << euclidTemp;
	euclidFile.close();

	cout << "Done!!!" << endl;
}

//Heuristic metho to find euclidean threshold for findAllRowMultiCsv's parameter
float euclideanHeuristic(
	vector< vector<int> > arr,  // an array data input
	int d1,						// dimension d1
	int d2,						// dimension d2
	int col,					// colunm to select data. T2 or T3 must be selected
	int maxCount				// maximum of random row
	)
{
	cout << "Function find euclidean max" << endl;
	vector< vector<int> > randomNewArr; // array to store new row candidate
	float euclidMax = -1;				// init for euclidean to find
	if (col != 1 && col != 2)
	{
		cout << "Function stop because column error!" << endl;
		return euclidMax;
	}

	if (d1 < 1 || d2 < 1 || arr.size() < 1 || (unsigned)(d1 + d2) > arr.size())
	{
		cout << "Function stop because dimension error!" << endl;
		return euclidMax;
	}

	int ind = 0;

	// loop for enough maximum data to check
	cout << "Loop to find euclidean max ..." << endl;
	while (ind <= maxCount)
	{
		cout << "Loop: " << ind << endl;
		/*find random value that not appear from old random*/
		int randomNum1 = randomInRange(0, arr.size());
		cout << "Random number 1: " << randomNum1 << endl;
		int randomNum2 = randomInRange(0, arr.size());
		cout << "Random number 2: " << randomNum1 << endl;
		if (randomNum1 == randomNum2)
		{
			cout << "Random number 1 is equal to random number 2, then ignore this case!" << endl;
			continue;
		}

		vector<int> row1 = createRowCandidate(arr, ind, randomNum1, d1, d2, col);
		vector<int> row2 = createRowCandidate(arr, ind, randomNum2, d1, d2, col);

		if (row1.size() < 1 || row2.size() < 1 || row1.size() != row2.size())
		{
			cout << "Row data 1 or row data 2 error, then ignore this case!" << endl;
			continue;
		}

		float euclidTemp = distEuclidean(row1, row2, d1); // euclidate temporatory
		cout << "Temporatory euclidean:" << euclidTemp << endl;
		ind++;
		if (euclidTemp != -1 && euclidTemp > euclidMax) // check for max value
		{
			cout << "Update euclidean max:" << euclidTemp << endl;
			euclidMax = euclidTemp;						// update euclidean max value
		}
	}

	cout << "Found euclidean max: " << euclidMax << endl;

	// return max euclidean value
	return euclidMax;
}


double dblTimeNow()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)tv.tv_sec + ((double)tv.tv_usec*1.0e-6));
}