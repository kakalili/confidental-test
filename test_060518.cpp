#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <set>
#include <iterator>
#include <math.h>
#include <algorithm>
#include <sstream>

using namespace std;
/*global variable*/

int notSatisfiedCount = 0; // global counter for continuous row not satisfied euclid condition

vector< vector<int> > allNewData; //store discovery data from multiple csv file

int nDataGlobal = 0; //Number of row data to earn from multiple csv file

/*Calculate euclidean distance from two array arr1 and arr2 with first d1 elements (ignore primary element)*/
double distEuclidean(vector<int> arr1, vector<int> arr2, int d1);
/*parse csv data from input filename to an array data*/
vector< vector<int> > inputCsvToArray(const char *filename);
/*Create a new row candidate from array data to put into euclid check
- arr: input array of data
- insertInd: current index of row on new file
- j: a random number
- d1: demension d1
- d2: demension d2
- column: column to select data. T2 or T3 must be selected.
*/
vector<int> createRowCandidate(vector< vector<int> > arr, int insertInd, int j, int d1, int d2, int column);
/*Append new row to a csv file with name filename*/
void appendRowToCsvFile(vector<int> row, const char *fileName);
/*Make a random number in range min and max value*/
int randomInRange(int min, int max);
/*Find all euclidean satisfied row from an array data
- arr: an array data input
- d1: demension d1
- d2: demension d2
- col: colunm to select data. T2 or T3 must be selected
- euclid: using only one file processing. threshold euclidean to check
- nultiFile: flag to indicate process one or more csv file. True: multiple csv file, otherwise: one csv file
*/
vector< vector<int> > findAllRow(vector< vector<int> > arr, int d1, int d2, int col, int euclid, bool multiFile);
/*Parse parameter from txt file to array of parameter*/
vector<string> readParamTxt(const char *filename);
/*Convert string to integer value*/
int convertStrToCol(string strColumn);
/*Find all euclidean satisfied row from an array data from specified parameter*/
void findAllRowMultiCsv(vector<string> paramArr);
/*Heuristic metho to find euclidean threshold for findAllRowMultiCsv's parameter
- arr: an array data input
- d1: demension d1
- d2: demension d2
- col: colunm to select data. T2 or T3 must be selected
- maxCount: maximum of random row
- outputFile: filename to push random row input it
*/
float euclideanHeuristic(vector< vector<int> > arr, int d1, int d2, int col, int maxCount, const char *outputFile);


int main(int argc, char* argv[])
{
	// Check the number of parameters
	if (argc != 7 && argc != 2) {
		// Tell the user how to run the program
		std::cerr << "Usage: one of two method bellow:" << std::endl
			<< "1/" << std::endl << argv[0] << " csv_input_file column_option d1 d2 euclidean_min csv_output_file" << std::endl
			<< "\t - csv_input_file: name of csv input file" << std::endl
			<< "\t - column_option: selected column to read data.(Option: T2 or T3)" << std::endl
			<< "\t - d1: Demension d1" << std::endl
			<< "\t - d2: Demension d2" << std::endl
			<< "\t - euclidean_min: Euclidean distance" << std::endl
			<< "\t - csv_output_file: name of csv output file" << std::endl
			<< "\t for example: ./test.o abc.csv T2 10 5 2.5 abc_new.csv" << std::endl
			<< "2/" << std::endl << argv[0] << " param_file" << std::endl
			<< "\t param_file: list of parameter with format:" << std::endl
			<< "\t - DataColumn: column_option (T2 or T3)" << std::endl
			<< "\t - Data1: d1" << std::endl
			<< "\t - Data2: d2" << std::endl
			<< "\t - nData: number of obtain data" << std::endl
			<< "\t - Euclidean_start: value of init euclidean" << std::endl
			<< "\t - OutputFilename: csv_output_file" << std::endl
			<< "\t - InputCSVFileName1: csv_input_file 1" << std::endl
			<< "\t - ......." << std::endl
			<< "\t - InputCSVFileName8: csv_input_file 8" << std::endl
			<< "\t for example:" << std::endl
			<< "\t\t DataColumn: T2" << std::endl
			<< "\t\t Data1: 10" << std::endl
			<< "\t\t Data2: 5" << std::endl
			<< "\t\t nData: 1000" << std::endl
			<< "\t\t Euclidean_start: 100" << std::endl
			<< "\t\t OutputFilename: abc_new.csv" << std::endl
			<< "\t\t InputCSVFileName1: abc_1.csv" << std::endl
			<< "\t\t InputCSVFileName2: abc_2.csv" << std::endl
			<< "\t\t InputCSVFileName3: abc_3.csv" << std::endl
			<< "\t\t InputCSVFileName4: abc_4.csv" << std::endl
			<< "\t\t InputCSVFileName5: abc_5.csv" << std::endl
			<< "\t\t InputCSVFileName6: abc_6.csv" << std::endl
			<< "\t\t InputCSVFileName7: abc_7.csv" << std::endl
			<< "\t\t InputCSVFileName8: abc_8.csv" << std::endl;
		;
		return 1;
	}

	if (argc == 7) //for one csv file processing
	{
		vector<int> oldRandom;
		const char *inputFile = argv[1];
		const char *outputFile = argv[6];
		string eulidMin = argv[5];
		string d1str = argv[3];
		string d2str = argv[4];

		string selectColumnStr = argv[2];
		int d1 = atoi(d1str.c_str());
		int d2 = atoi(d2str.c_str());
		int selectedCol = convertStrToCol(selectColumnStr);

		double euclid = atof(eulidMin.c_str());
		//read data from csv file
		vector< vector<int> > arr = inputCsvToArray(inputFile);
		/*
		for (size_t j = 0; j < arr.size(); ++j)
		{
		appendRowToCsvFile(arr[j], outputFile);
		}


		vector<int> row = createRowCandidate(arr, 0, 5, d1, d2, selectedCol);
		for (size_t j = 0; j < row.size(); ++j)
		{
		cout << row[j]; // (separate fields by |)
		}
		cout << std::endl;
		*/

		vector< vector<int> > reArr = findAllRow(arr, d1, d2, selectedCol, euclid, false);
		if (reArr.size() > 0)
		{

			for (size_t j = 0; j < reArr.size(); ++j)
			{
				appendRowToCsvFile(reArr[j], outputFile);
			}
		}

		/*test euclideanHeuristic function*/
		/*
		float euclidV = euclideanHeuristic(arr, d1, d2, selectedCol, 1000, outputFile);
		cout << euclidV;
		*/
	}

	if (argc == 2) //for multiple csv file processing
	{
		string paramFile = argv[1];
		vector<string> paramLists = readParamTxt(paramFile.c_str()); //reading parameter list from file
		if (paramLists.size() >= 7)
		{
			findAllRowMultiCsv(paramLists);
		}
	}

	return 0;

}

//calculate Euclidean between array
double distEuclidean(vector<int> arr1, vector<int> arr2, int d1)
{
	float Sum = 0.0f;

	if (arr1.size() <= d1 || arr2.size() <= d1 || d1 < 1)
	{
		return -1;
	}


	for (int i = 1; i <= d1; i++)
	{
		Sum = Sum + pow((arr1[i] - arr2[i]), 2.0);
	}

	return sqrt(Sum);
}

/*parse csv data from input filename to an array data*/
vector< vector<int> > inputCsvToArray(const char *filename)
{
	ifstream in(filename);
	string line, field;
	int temp;
	vector< vector<int> > arr;  // the 2D array
	vector<int> v;                // array of values for one line only
	int i = 0;
	while (getline(in, line))    // get next line in file
	{
		if (i == 0) //ignore first line
		{
			i++;
			continue;
		}
		v.clear();
		stringstream ss(line);

		while (getline(ss, field, ','))  // break line into comma delimitted fields
		{

			temp = atoi(field.c_str());	//convert string to int number
			if (temp > 0)
			{
				v.push_back(temp);  // add each field to the 1D array
			}
		}
		if (v.size() > 2)
		{
			arr.push_back(v);  // add the 1D array to the 2D array
		}

	}

	return arr;
}

//j in (0, csv_length), 
//insertInd: insert index, 
//colum: 1: t2, 2: t3
/*Create a new row candidate from array data to put into euclid check
- arr: input array of data
- insertInd: current index of row on new file
- j: a random number
- d1: demension d1
- d2: demension d2
- column: column to select data. T2 or T3 must be selected.
*/
vector<int> createRowCandidate(vector< vector<int> > arr, int insertInd, int j, int d1, int d2, int column)
{
	vector<int> row;  // the output array
	//check ahead
	//TODO
	if (column != 1 && column != 2)
	{
		return row;
	}

	if (j < 0 || d1 < 1 || d2 < 1 || arr.size() < 1 || j + d1 + d2 > arr.size())
	{
		return row;
	}
	//insert index to row
	row.push_back(insertInd);

	//append next d1 elements from colum to row
	for (int i = 1; i <= d1; i++)
	{
		row.push_back(arr[j + i - 1][column]);
	}

	//append next d2 elements from colum to row
	for (int i = 1; i <= d2; i++)
	{
		row.push_back(arr[j + d1 + i - 1][column]);
	}

	//append last element
	row.push_back(arr[j][0]);

	return row;
}

/*Append new row to a csv file with name filename*/
void appendRowToCsvFile(vector<int> row, const char *fileName)
{
	ofstream outputFile;
	outputFile.open(fileName, std::ofstream::out | std::ofstream::app);
	for (size_t j = 0; j < row.size(); ++j)
	{
		if (j == row.size() - 1)
		{
			outputFile << row[j] << std::endl; // (separate fields by |)
		}
		else
		{
			outputFile << row[j] << ","; // (separate fields by |)
		}
	}
	outputFile.close();
}

/*create random number in range*/
int randomInRange(int min, int max)
{
	return min + (std::rand() % (max - min + 1));
}

/*Find all euclidean satisfied row from an array data
- arr: an array data input
- d1: demension d1
- d2: demension d2
- col: colunm to select data. T2 or T3 must be selected
- euclid: using only one file processing. threshold euclidean to check
- nultiFile: flag to indicate process one or more csv file. True: multiple csv file, otherwise: one csv file
- return: found row list
*/
vector< vector<int> > findAllRow(vector< vector<int> > arr, int d1, int d2, int col, int euclid, bool multiFile = false)
{
	vector< vector<int> > results; //array to store new row
	//if multiple csv processing, assign before new satisfied row collection to results
	if (multiFile && allNewData.size() > 0)
	{
		results = allNewData;
	}
	//check for column
	if (col != 1 && col != 2)
	{
		return results;
	}

	// check for demension
	if (d1 < 1 || d2 < 1 || arr.size() < 1 || d1 + d2 > arr.size() || euclid < 0)
	{
		return results;
	}

	double euclidTemp; //euclidean temporatory
	int ind = 0; //index of new row
	// if multiple csv file process then init ind to size of before row list
	if (multiFile && allNewData.size() > 0)
	{
		ind = allNewData.size();
	}
	vector<int> row; //tempratory new row
	vector<int> oldRand; //list of taken random j before
	bool firstRand = true; // first time random

	//check
	bool stopFlag = false; //flag stop: if true then stop while loop
	//multi csv file 1000 row not satisfied check
	bool currentSatified = false; //flag to check continuous of euclid statement, using only for multiple csv file process
	while (!stopFlag)
	{
		if (oldRand.size() == arr.size()) //if random list fulfill in origin array
		{
			stopFlag = true;
			break;
		}

		bool isRand = false; //flag to check first time random
		int rando;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size()); //random number in range
			if (firstRand) //check firstime random
			{
				oldRand.push_back(rando); //push random value to list taken random
				firstRand = false; //update firstime random
				isRand = true; //update 
			}
			else //if not firstime random
			{
				//check new random existing in list taken radom
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}

		row = createRowCandidate(arr, ind, rando, d1, d2, col); //create new row candidate with found randoms
		if (row.size() < 1) //if row cannot created
		{
			stopFlag = true; //stop 
			break;
		}
		if (ind == 0) //if first time insert new row to new csv file
		{
			results.push_back(row);
			ind++;
		}
		else
		{
			bool insertPosible = true; //flag to check posible insert new row candidate to list data
			// loop to check euclidean from new row with before list data
			for (size_t j = 0; j < results.size(); ++j)
			{
				euclidTemp = distEuclidean(row, results[j], d1); //caculate euclid distance 
				if (euclidTemp == -1 || (euclidTemp != -1 && euclidTemp < euclid)) //check if every before row has an euclid distance larger than euclidean min
				{
					insertPosible = false;
					break;
				}
			}

			if (insertPosible) //if insert posible
			{
				results.push_back(row);
				ind++;
				//if found row satisfied then update condition and satisfied counter
				if (multiFile) //if multiple csv processing
				{
					currentSatified = true; //reset euclidean satisfied state
					notSatisfiedCount = 0; //reset continuous row that not satisfied
					if (results.size() >= nDataGlobal) //check for enough data wanna to taken
					{
						stopFlag = true;
						break;
					}
				}
			}
			else //if insert imposible
			{
				if (!multiFile) //if one csv file processing
				{
					stopFlag = true; //stop while loop imtermediate
					break;
				}
				else // if multiple csv process
				{
					notSatisfiedCount++; //increate number row that not satisfied euclid condition
					if (currentSatified)
					{
						currentSatified = false; //update satisfied euclid condition flag
					}
					if (notSatisfiedCount == 1000)
					{
						stopFlag = true;
						break;
					}
				}

			}
		}
	}
	//appendRowToCsvFile(oldRand, "random.csv");
	return results;
}

/*Parse parameter from file*/
vector<string> readParamTxt(const char *filename)
{
	string line;
	vector<string> paramList;
	ifstream inputFile(filename);

	while (getline(inputFile, line))
	{
		istringstream ss(line);
		string var1, var2;
		ss >> var1 >> var2;
		if (!var2.empty()) //check value of param
		{
			paramList.push_back(var2);
		}
		//cout << var1 << "\t" << var2 << endl;
	}
	return paramList;
}

/*convert string to integer for column*/
int convertStrToCol(string strColumn)
{
	int colReturn = -1;
	if (strColumn.compare("T2") == 0)
	{
		colReturn = 1;
	}

	if (strColumn.compare("T3") == 0)
	{
		colReturn = 2;
	}
	return colReturn;
}

/*Find all euclidean satisfied row from an array data from specified parameter*/
void findAllRowMultiCsv(vector<string> paramArr)
{
	if (paramArr.size() < 7) //check param list
	{
		return;
	}
	int col = convertStrToCol(paramArr[0]); //convert column to number
	if (col == -1)
	{
		return;
	}
	int d1 = atoi(paramArr[1].c_str()); //convert d1 from param
	int d2 = atoi(paramArr[2].c_str()); //convert d2 from param
	int nData = atoi(paramArr[3].c_str()); ////convert nData from param
	nDataGlobal = nData; //assign nData to global variable to check in other function
	double euclidean_start = atof(paramArr[4].c_str()); //convert euclidean start from param
	if (d1 <= 0 || d2 <= 0 || nData < 1 || euclidean_start <= 0)
	{
		return;
	}
	const char *outputFile = paramArr[5].c_str(); //output file to write discovery data into
	//read multi csv file into only one array data
	vector< vector<int> > allRawData; //array of raw data collect from list input csv file
	bool firstInsert = true; //flag to check first time inset in to raw data array
	/*collect all raw data from list input file*/
	for (int i = 6; i < paramArr.size(); i++)
	{
		vector< vector<int> > tempRawData = inputCsvToArray(paramArr[i].c_str()); //parse raw data from a csv file
		if (tempRawData.size() > 0) //check is has data
		{
			if (firstInsert) // first time insert
			{
				firstInsert = false;
				allRawData = tempRawData; //assign 
			}
			else
			{
				allRawData.insert(allRawData.end(), tempRawData.begin(), tempRawData.end()); //append new raw data to allRawData
			}
		}
	}


	bool stop = false; //flag to check stop
	double euclidTemp = euclidean_start; //euclidean start
	vector< vector<int> > reTempArr; //array to store new discovery data
	/*find nData by adjust euclidean*/
	while (!stop)
	{
		reTempArr = findAllRow(allRawData, d1, d2, col, euclidTemp, true); //find satisfied row from all raw data with euclid value
		if (reTempArr.size() > 1) // if new data not empty
		{
			allNewData = reTempArr; //update all new discovery data list
			if (allNewData.size() >= nData) //if enough data wanna to taken
			{
				stop = true;
				break;
			}
		}
		//if not enough data to earn, then update euclid value
		notSatisfiedCount = 0; //reset number of not satisfied row
		euclidTemp = euclidTemp * 0.9; //update euclid value
	}

	// write all new discovery data to csv file
	if (allNewData.size() > 0)
	{

		for (size_t j = 0; j < allNewData.size(); ++j)
		{
			appendRowToCsvFile(allNewData[j], outputFile);
		}
	}

	//write euclid min to file
	ofstream euclidFile;
	euclidFile.open("euclidean_min.txt", std::ofstream::out);
	euclidFile << euclidTemp;
	euclidFile.close();
}

/*Heuristic metho to find euclidean threshold for findAllRowMultiCsv's parameter
- arr: an array data input
- d1: demension d1
- d2: demension d2
- col: colunm to select data. T2 or T3 must be selected
- maxCount: maximum of random row
- outputFile: filename to push random row input it
*/
float euclideanHeuristic(vector< vector<int> > arr, int d1, int d2, int col, int maxCount, const char *outputFile)
{
	vector< vector<int> > randomNewArr; //array to store new row candidate
	float euclidMax = -1; //init for euclidean to find
	if (col != 1 && col != 2)
	{
		return euclidMax;
	}

	if (d1 < 1 || d2 < 1 || arr.size() < 1 || d1 + d2 > arr.size())
	{
		return euclidMax;
	}

	double euclidTemp; //euclidate temporatory
	int ind = 0;
	vector<int> row;
	vector<int> oldRand;
	bool firstRand = true;

	//check
	// loop for enough maximum data to check
	while (ind <= maxCount)
	{
		/*find random value that not appear from old random*/
		if (oldRand.size() == arr.size()) //if random list fulfill in origin array
		{
			break;
		}
		bool isRand = false;
		int rando;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size());
			if (firstRand)
			{
				oldRand.push_back(rando);
				firstRand = false;
				isRand = true;
			}
			else
			{
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}

		//create a new row
		row = createRowCandidate(arr, ind, rando, d1, d2, col);
		if (row.size() < 1) //if cannot create new row, then continue 
		{
			continue;
		}
		else // if new row taken
		{
			if (ind == 0) //if first time insert new row to new csv file
			{
				randomNewArr.push_back(row);
				ind++;
			}
			else
			{
				/*find euclidean max value*/
				for (size_t j = 0; j < randomNewArr.size(); ++j)
				{
					euclidTemp = distEuclidean(row, randomNewArr[j], d1);
					if (euclidTemp != -1 && euclidTemp > euclidMax) //check for max value
					{
						euclidMax = euclidTemp; //update euclidean max value
					}
				}
				randomNewArr.push_back(row);
				ind++;
			}
			//write new row to csv file
			appendRowToCsvFile(row, outputFile);
		}
	}

	//return max euclidean value
	return euclidMax;
}