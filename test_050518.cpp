#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <set>
#include <iterator>
#include<math.h>
#include <algorithm>
#include <sstream>

using namespace std;
/*global variable*/
int notSatisfiedCount = 0; // global counter for continuous row not satisfied euclid condition
vector< vector<int> > allNewData; //
int nDataGlobal = 0;

double distEuclidean(vector<int> arr1, vector<int> arr2, int d1);
vector< vector<int> > inputCsvToArray(const char *filename);
vector<int> createRowCandidate(vector< vector<int> > arr, int insertInd, int j, int d1, int d2, int column);
void appendRowToCsvFile(vector<int> row, const char *fileName);
int randomInRange(int min, int max);
vector< vector<int> > findAllRow(vector< vector<int> > arr, int d1, int d2, int col, int euclid, bool multiFile);
vector<string> readParamTxt(const char *filename);
int convertStrToCol(string strColumn);
vector< vector<int> > findAllRowMultiCsv(vector<string> paramArr);

int main(int argc, char* argv[])
{
	// Check the number of parameters
	if (argc != 7 && argc != 2) {
		// Tell the user how to run the program
		std::cerr << "Usage: one of two method bellow:" << std::endl
			<< "1/" << std::endl << argv[0] << " csv_input_file column_option d1 d2 euclidean_min csv_output_file" << std::endl
			<< "\t - csv_input_file: name of csv input file" << std::endl
			<< "\t - column_option: selected column to read data.(Option: T2 or T3)" << std::endl
			<< "\t - d1: Demension d1" << std::endl
			<< "\t - d2: Demension d2" << std::endl
			<< "\t - euclidean_min: Euclidean distance" << std::endl
			<< "\t - csv_output_file: name of csv output file" << std::endl
			<< "\t for example: ./test.o abc.csv T2 10 5 2.5 abc_new.csv" << std::endl
			<< "2/" << std::endl << argv[0] << " param_file" << std::endl
			<< "\t param_file: list of parameter with format:" << std::endl
			<< "\t - DataColumn: column_option (T2 or T3)" << std::endl
			<< "\t - Data1: d1" << std::endl
			<< "\t - Data2: d2" << std::endl
			<< "\t - nData: number of obtain data" << std::endl
			<< "\t - Euclidean_start: value of init euclidean" << std::endl
			<< "\t - OutputFilename: csv_output_file" << std::endl
			<< "\t - InputCSVFileName1: csv_input_file 1" << std::endl
			<< "\t - ......." << std::endl
			<< "\t - InputCSVFileName8: csv_input_file 8" << std::endl
			<< "\t for example:" << std::endl
			<< "\t\t DataColumn: T2" << std::endl
			<< "\t\t Data1: 10" << std::endl
			<< "\t\t Data2: 5" << std::endl
			<< "\t\t nData: 1000" << std::endl
			<< "\t\t Euclidean_start: 100" << std::endl
			<< "\t\t OutputFilename: abc_new.csv" << std::endl
			<< "\t\t InputCSVFileName1: abc_1.csv" << std::endl
			<< "\t\t InputCSVFileName2: abc_2.csv" << std::endl
			<< "\t\t InputCSVFileName3: abc_3.csv" << std::endl
			<< "\t\t InputCSVFileName4: abc_4.csv" << std::endl
			<< "\t\t InputCSVFileName5: abc_5.csv" << std::endl
			<< "\t\t InputCSVFileName6: abc_6.csv" << std::endl
			<< "\t\t InputCSVFileName7: abc_7.csv" << std::endl
			<< "\t\t InputCSVFileName8: abc_8.csv" << std::endl;
			;
		return 1;
	}
	if (argc == 7)
	{
		vector<int> oldRandom;
		const char *inputFile = argv[1];
		const char *outputFile = argv[6];
		string eulidMin = argv[5];
		string d1str = argv[3];
		string d2str = argv[4];

		string selectColumnStr = argv[2];
		int d1 = atoi(d1str.c_str());
		int d2 = atoi(d2str.c_str());
		int selectedCol = convertStrToCol(selectColumnStr);

		double euclid = atof(eulidMin.c_str());
		//read data from csv file
		vector< vector<int> > arr = inputCsvToArray(inputFile);
		/*
		for (size_t j = 0; j < arr.size(); ++j)
		{
		appendRowToCsvFile(arr[j], outputFile);
		}


		vector<int> row = createRowCandidate(arr, 0, 5, d1, d2, selectedCol);
		for (size_t j = 0; j < row.size(); ++j)
		{
		cout << row[j]; // (separate fields by |)
		}
		cout << std::endl;
		*/

		vector< vector<int> > reArr = findAllRow(arr, d1, d2, selectedCol, euclid, false);
		if (reArr.size() > 0)
		{

			for (size_t j = 0; j < reArr.size(); ++j)
			{
				appendRowToCsvFile(reArr[j], outputFile);
			}
		}
	}

	if (argc == 2)
	{
		string paramFile = argv[1];
		vector<string> paramLists = readParamTxt(paramFile.c_str());
		if (paramLists.size() >= 7)
		{
			findAllRowMultiCsv(paramLists);
		}
	}
	
	return 0;

}

//calculate Euclidean between array
double distEuclidean(vector<int> arr1, vector<int> arr2, int d1)
{
	float Sum = 0.0f;

	if (arr1.size() <= d1 || arr2.size() <= d1 || d1 < 1)
	{
		return -1;
	}


	for (int i = 1; i <= d1; i++)
	{
		Sum = Sum + pow((arr1[i] - arr2[i]), 2.0);
	}

	return sqrt(Sum);
}


vector< vector<int> > inputCsvToArray(const char *filename)
{
	ifstream in(filename);
	string line, field;
	int temp;
	vector< vector<int> > arr;  // the 2D array
	vector<int> v;                // array of values for one line only
	int i = 0;
	while (getline(in, line))    // get next line in file
	{
		if (i == 0) //ignore first line
		{
			i++;
			continue;
		}
		v.clear();
		stringstream ss(line);

		while (getline(ss, field, ','))  // break line into comma delimitted fields
		{
			temp = atoi(field.c_str());
			v.push_back(temp);  // add each field to the 1D array
		}

		arr.push_back(v);  // add the 1D array to the 2D array
	}

	return arr;
}

//j in (0, csv_length), 
//insertInd: insert index, 
//colum: 1: t2, 2: t3
vector<int> createRowCandidate(vector< vector<int> > arr, int insertInd, int j, int d1, int d2, int column)
{
	vector<int> row;  // the output array
	//check ahead
	//TODO
	if (column != 1 && column != 2)
	{
		return row;
	}

	if (j < 0 || d1 < 1 || d2 < 1 || arr.size() < 1 || j + d1 + d2 > arr.size())
	{
		return row;
	}
	//

	row.push_back(insertInd);

	for (int i = 1; i <= d1; i++)
	{
		row.push_back(arr[j + i - 1][column]);
	}
	for (int i = 1; i <= d2; i++)
	{
		row.push_back(arr[j + d1 + i - 1][column]);
	}
	row.push_back(arr[j][0]);

	return row;
}

void appendRowToCsvFile(vector<int> row, const char *fileName)
{
	ofstream outputFile;
	outputFile.open(fileName, std::ofstream::out | std::ofstream::app);
	for (size_t j = 0; j < row.size(); ++j)
	{
		if (j == row.size() - 1)
		{
			outputFile << row[j] << std::endl; // (separate fields by |)
		}
		else
		{
			outputFile << row[j] << ","; // (separate fields by |)
		}
	}
	outputFile.close();
}

int randomInRange(int min, int max)
{
	return min + (std::rand() % (max - min + 1));
}

vector< vector<int> > findAllRow(vector< vector<int> > arr, int d1, int d2, int col, int euclid, bool multiFile = false)
{
	vector< vector<int> > results;
	if (multiFile && allNewData.size() > 0)
	{
		results = allNewData;
	}
	if (col != 1 && col != 2)
	{
		return results;
	}

	if (d1 < 1 || d2 < 1 || arr.size() < 1 || d1 + d2 > arr.size() || euclid < 0)
	{
		return results;
	}

	double euclidTemp;
	int ind = 0;
	vector<int> row;
	vector<int> oldRand;
	bool firstRand = true;

	//check
	bool stopFlag = false;
	//multi csv file 1000 row not satisfied check
	bool currentSatified = false;
	while (!stopFlag)
	{
		if (oldRand.size() == arr.size()) //if random list fulfill in origin array
		{
			stopFlag = true;
			break;
		}
		bool isRand = false;
		int rando;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size());
			if (firstRand)
			{
				oldRand.push_back(rando);
				firstRand = false;
				isRand = true;
			}
			else
			{
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}

		row = createRowCandidate(arr, ind, rando, d1, d2, col);
		if (row.size() < 1)
		{
			stopFlag = true;
			break;
		}
		if (ind == 0) //if first time insert new row to new csv file
		{
			//appendRowToCsvFile(row, outputFile);
			results.push_back(row);
			ind++;
		}
		else
		{
			bool insertPosible = true;
			for (size_t j = 0; j < results.size(); ++j)
			{
				euclidTemp = distEuclidean(row, results[j], d1);
				if (euclidTemp == -1 || (euclidTemp != -1 && euclidTemp < euclid)) //check if every before row has an euclid distance larger than euclidean min
				{
					insertPosible = false;
					break;
				}
			}

			if (insertPosible)
			{
				results.push_back(row);
				ind++;
				//if found row satisfied then update condition and satisfied counter
				if (multiFile)
				{
					currentSatified = true;
					notSatisfiedCount = 0;
					if (results.size() >= nDataGlobal)
					{
						stopFlag = true;
						break;
					}
				}
			}
			else 
			{
				if (!multiFile)
				{
					stopFlag = true;
					break;
				}
				else
				{
					notSatisfiedCount++;
					if (currentSatified)
					{
						currentSatified = false;
					}
					if (notSatisfiedCount == 1000)
					{
						stopFlag = true;
						break;
					}
				}
				
			}
		}
	}
	//appendRowToCsvFile(oldRand, "random.csv");
	return results;
}

vector<string> readParamTxt(const char *filename)
{
	string line;
	vector<string> paramList;
	ifstream inputFile(filename);

	while (getline(inputFile, line))
	{
		istringstream ss(line);
		string var1, var2;
		ss >> var1 >> var2;
		if (!var2.empty())
		{
			paramList.push_back(var2);
		}
		//cout << var1 << "\t" << var2 << endl;
	}
	return paramList;
}

int convertStrToCol(string strColumn)
{
	int colReturn = -1;
	if (strColumn.compare("T2") == 0)
	{
		colReturn = 1;
	}

	if (strColumn.compare("T3") == 0)
	{
		colReturn = 2;
	}
	return colReturn;
}

vector< vector<int> > findAllRowMultiCsv(vector<string> paramArr)
{
	vector< vector<int> > data;
	if (paramArr.size() < 7)
	{
		return data;
	}
	int col = convertStrToCol(paramArr[0]);
	if (col == -1)
	{
		return data;
	}
	int d1 = atoi(paramArr[1].c_str());
	int d2 = atoi(paramArr[2].c_str());
	int nData = atoi(paramArr[3].c_str());
	nDataGlobal = nData;
	double euclidean_start = atof(paramArr[4].c_str());
	if (d1 < 0 || d2 < 0 || nData < 1 || euclidean_start <= 0)
	{
		return data;
	}
	const char *outputFile = paramArr[5].c_str();
	//read multi csv file into only one array data
	vector< vector<int> > allRawDataTemp;
	bool firstInsert = true;
	for (int i = 6; i < paramArr.size(); i++)
	{
		vector< vector<int> > tempRawData = inputCsvToArray(paramArr[i].c_str());
		if (tempRawData.size() > 0)
		{
			if (firstInsert) 
			{
				firstInsert = false;
				allRawDataTemp = tempRawData;
			}
			else
			{
				allRawDataTemp.insert(allRawDataTemp.end(), tempRawData.begin(), tempRawData.end());
			}
		}
	}

	
	bool stop = false;
	double euclidTemp = euclidean_start;
	vector< vector<int> > reTempArr;
	/*find nData by adjust euclidean*/
	while (!stop)
	{
		reTempArr = findAllRow(allRawDataTemp, d1, d2, col, euclidTemp, true);
		if (reTempArr.size() > 1)
		{
			allNewData = reTempArr;
			if (allNewData.size() >= nData)
			{
				stop = true;
				break;
			}
		}
		notSatisfiedCount = 0;
		euclidTemp = euclidTemp * 0.9;		
	}
	
	if (allNewData.size() > 0)
	{

		for (size_t j = 0; j < allNewData.size(); ++j)
		{
			appendRowToCsvFile(allNewData[j], outputFile);
		}
	}

	//write euclid to file
	ofstream euclidFile;
	euclidFile.open("euclidean_min.txt", std::ofstream::out);
	euclidFile << euclidTemp;
	euclidFile.close();
}

float euclideanHeuristic(vector< vector<int> > arr, int d1, int d2, int col, int maxCount, const char *outputFile)
{
	vector< vector<int> > randomNewArr;
	float euclidMax = -1;
	if (col != 1 && col != 2)
	{
		return euclidMax;
	}

	if (d1 < 1 || d2 < 1 || arr.size() < 1 || d1 + d2 > arr.size())
	{
		return euclidMax;
	}

	double euclidTemp;
	int ind = 0;
	vector<int> row;
	vector<int> oldRand;
	bool firstRand = true;

	//check
	//multi csv file 1000 row not satisfied check
	bool currentSatified = false;
	while (ind <= maxCount)
	{
		if (oldRand.size() == arr.size()) //if random list fulfill in origin array
		{
			break;
		}
		bool isRand = false;
		int rando;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size());
			if (firstRand)
			{
				oldRand.push_back(rando);
				firstRand = false;
				isRand = true;
			}
			else
			{
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}

		row = createRowCandidate(arr, ind, rando, d1, d2, col);
		if (row.size() < 1)
		{
			continue;
		}
		else
		{
			if (ind == 0) //if first time insert new row to new csv file
			{
				randomNewArr.push_back(row);
				ind++;
			}
			else
			{
				for (size_t j = 0; j < randomNewArr.size(); ++j)
				{
					euclidTemp = distEuclidean(row, randomNewArr[j], d1);
					if (euclidTemp != -1 && euclidTemp > euclidMax) //check if every before row has an euclid distance larger than euclidean min
					{
						euclidMax = euclidTemp;
					}
				}
				randomNewArr.push_back(row);
				ind++;
			}
			appendRowToCsvFile(row, outputFile);
		}
	}

	return euclidMax;
}