#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <set>
#include <iterator>
#include<math.h>
#include <algorithm>

using namespace std;


//calculate Euclidean between array
double distEuclidean(vector<int> arr1, vector<int> arr2, int d1)
{
	double Sum;
	double distance;

	if (arr1.size() <= d1 || arr2.size() <= d1 || d1 < 1)
	{
		return -1;
	}


	for (int i = 1; i <= d1; i++)
	{
		Sum = Sum + pow((arr1[i] - arr2[i]), 2.0);
		distance = sqrt(Sum);
	}

	return distance;
}


vector< vector<int> > inputCsvToArray(char *filename)
{
	ifstream in(filename);
	string line, field;
	int temp;
	vector< vector<int> > arr;  // the 2D array
	vector<int> v;                // array of values for one line only
	int i = 0;
	while (getline(in, line))    // get next line in file
	{
		if (i == 0) //ignore first line
		{
			i++;
			continue;
		}
		v.clear();
		stringstream ss(line);

		while (getline(ss, field, ','))  // break line into comma delimitted fields
		{
			temp = atoi(field.c_str());
			v.push_back(temp);  // add each field to the 1D array
		}

		arr.push_back(v);  // add the 1D array to the 2D array
	}

	return arr;
}

//j in (0, csv_length), 
//insertInd: insert index, 
//colum: 1: t2, 2: t3
vector<int> createRowCandidate(vector< vector<int> > arr, int insertInd, int j, int d1, int d2, int column)
{
	vector<int> row;  // the output array
	//check ahead
	//TODO
	if (column != 1 && column != 2)
	{
		return row;
	}

	if (j < 0 || d1 < 1 || d2 < 1 || arr.size() < 1 || j + d1 + d2 > arr.size())
	{
		return row;
	}
	//

	row.push_back(insertInd);

	for (int i = 1; i <= d1; i++)
	{
		row.push_back(arr[j + i - 1][column]);
	}
	for (int i = 1; i <= d2; i++)
	{
		row.push_back(arr[j + d1 + i - 1][column]);
	}
	row.push_back(arr[j][0]);

	return row;
}

void appendRowToCsvFile(vector<int> row, const char *fileName)
{
	ofstream outputFile;
	outputFile.open(fileName, std::ofstream::out | std::ofstream::app);
	for (size_t j = 0; j < row.size(); ++j)
	{
		if (j == row.size() - 1)
		{
			outputFile << row[j] << std::endl; // (separate fields by |)
		}
		else
		{
			outputFile << row[j] << ","; // (separate fields by |)
		}
	}
	outputFile.close();
}

int randomInRange(int min, int max)
{
	return min + (std::rand() % (max - min + 1));
}

vector< vector<int> > findAllRow(vector< vector<int> > arr, int d1, int d2, int col, int euclid)
{
	vector< vector<int> > rerults;
	if (col != 1 && col != 2)
	{
		return rerults;
	}

	if (d1 < 1 || d2 < 1 || arr.size() < 1 || d1 + d2 > arr.size() || euclid < 0)
	{
		return rerults;
	}

	double euclidTemp;
	int ind = 0;
	vector<int> row;
	vector<int> oldRand;
	bool firstRand = true;

	//check
	bool flag = true;
	while (flag)
	{
		if (oldRand.size() == arr.size()) //if random list fulfill in origin array
		{
			flag = false;
			break;
		}
		bool isRand = false;
		int rando;
		while (!isRand)
		{
			rando = randomInRange(0, arr.size());
			if (firstRand)
			{
				oldRand.push_back(rando);
				firstRand = false;
				isRand = true;
			} 
			else
			{
				if (std::find(oldRand.begin(), oldRand.end(), rando) != oldRand.end()) {
					/* oldRand contains rando */
					continue;
				}
				else {
					/* oldRand does not contain rando */
					isRand = true;
					oldRand.push_back(rando);
				}
			}
		}
		
		row = createRowCandidate(arr, ind, rando, d1, d2, col);
		if (row.size() < 1)
		{
			flag = false;
			break;
		}
		if (ind == 0) //if first time insert new row to new csv file
		{
			//appendRowToCsvFile(row, outputFile);
			rerults.push_back(row);
			ind++;
		}
		else
		{
			bool insertPosible = true;
			for (size_t j = 0; j < rerults.size(); ++j)
			{
				euclidTemp = distEuclidean(row, rerults[j], d1);
				if (euclidTemp != -1 && euclidTemp < euclid) //check if every before row has an euclid distance larger than euclidean min
				{
					insertPosible = false;
					break;
				}
			}

			if (insertPosible)
			{
				rerults.push_back(row);
				ind++;

			}
		}
	}
	appendRowToCsvFile(oldRand, "random.csv");
	return rerults;
}

int main(int argc, char* argv[])
{
	// Check the number of parameters
	if (argc < 7) {
		// Tell the user how to run the program
		std::cerr << "Usage: " << argv[0] << " abc.csv T2 10 5 2.5 abc_new.csv" << std::endl;
		return 1;
	}

	int ind = 0;
	vector<int> oldRandom;
	char *inputFile = argv[1];
	const char *outputFile = argv[6];
	string eulidMin = argv[5];
	string d1str = argv[3];
	string d2str = argv[4];

	string selectColumnStr = argv[2];
	int d1 = atoi(d1str.c_str());
	int d2 = atoi(d2str.c_str());
	int selectedCol = atoi(selectColumnStr.c_str());
	double euclid = atof(eulidMin.c_str());
	//read data from csv file
	vector< vector<int> > arr = inputCsvToArray(inputFile);
	/*
	for (size_t j = 0; j < arr.size(); ++j)
	{
		appendRowToCsvFile(arr[j], outputFile);
	}
	
	
	vector<int> row = createRowCandidate(arr, 0, 5, d1, d2, selectedCol);
	for (size_t j = 0; j < row.size(); ++j)
	{
		cout << row[j]; // (separate fields by |)
	}
	cout << std::endl;
	*/

	vector< vector<int> > reArr = findAllRow(arr, d1, d2, selectedCol, euclid);
	if (reArr.size() > 0)
	{

		for (size_t j = 0; j < reArr.size(); ++j)
		{
			appendRowToCsvFile(reArr[j], outputFile);
		}
	}
	
	//appendRowToCsvFile(row, outputFile);
	return 0;

}