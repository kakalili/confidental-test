function X = nomalize(M)
    X = M(1:3000,2:181);
    m0 = min(X(isfinite(X(:))));   % get minimum in A
    m1 = max(X(isfinite(X(:))));   % get maximum in A
    X = X/(m1+m0 ) ;
end;