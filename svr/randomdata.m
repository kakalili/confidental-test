function [Xtrain, Ytrain, Xtest, Ytest] = randomdata(X, lengthX, colX, trunkRow, trunkCol )
    k = randperm(lengthX);

    Xtrain = X(k(1:trunkRow),1:trunkCol);

    Ytrain = X(k(1:trunkRow),trunkCol+1:colX);

    Xtest = X(k(trunkRow+1:lengthX),1:trunkCol);

    Ytest = X(k(trunkRow+1:lengthX),trunkCol+1:colX);
    csvwrite('trainx_0.txt',Xtrain)
    csvwrite('trainy_0.txt',Ytrain)
    csvwrite('testx_0.txt',Xtest)
    csvwrite('testy_0.txt',Ytest)
