%
% Simple demo for running the
% Multioutput Support Vector Rregression (MSVR)
%
% References:
% "Multioutput support vector regression for
% remote sensing biophysical parameter estimation"
% Tuia, D. and Verrelst, J. and Alonso, L. and Perez-Cruz, F. and Camps-Valls, G.
% IEEE Geoscience and Remote Sensing Letters 8 (4):804-808, 2011
%
%
% Camps-Valls, 2016(c)
% gcamps@uv.es
%

clear;
clc;
close all;

%% Load and prepare data

filename = '9000-data_150-inputs_30-outputs.csv';
M = csvread(filename)	% load csv data to M


%nomalize
X = M(1:9000,1:180);            % get meaning data from M

%m0 = min(X(isfinite(X(:))));  	% get minimum in X
%m1 = max(X(isfinite(X(:)))); 	% get maximum in X
%X = X/(m1+m0 ) ;                % normalize X

% Training an testing data split
Xtrain = X(1:8000,1:150);       % select training input data set
Ytrain = X(1:8000,151:180);     % select training output data set
Xtest = X(8001:9000,1:150);     % select training output data set
Ytest = X(8001:9000,151:180);   % select training output data set

%% Training with pairs Xtrain-Ytrain
% Parameters:
%   C: penalization (regularization) parameter
%   epsilon: tolerated error, defines the insensitivity zone
%   sigma: lengthscale of the RBF kernel
%   tol is a very small number (like 1e-15) for early stopping the
%   optimization of the IRWLS algorithm
ker = 'lin';
epsilon = 0.1;
C =  0.7;
sigma = 0.0005;
tol = 1e-10;

[Beta] = msvr(Xtrain,Ytrain,ker,C,epsilon,sigma,tol);

% Prediction on new test data Xtest
% Build the test kernel matrix
Ktest = kernelmatrix(ker,Xtest',Xtrain',sigma);
Ypred = Ktest*Beta;
csvwrite('predict_output2.csv', Ypred);

% Evaluate 
length = size(Ypred,1); % number row of Ypred
diff = sum(abs(1-Ypred/Ytest))/length;
disp(diff);